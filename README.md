# base-backend

Basic Express backend with user auth and TypeORM Postgres database.

## Setup

```
yarn
```

## Develop

```
yarn dev
```
