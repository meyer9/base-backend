// eslint-disable-next-line import/order
import dotenv from "dotenv";
dotenv.config();

import userRouter from "./api/user";
import { DatabaseConnection } from "./database";
import express from "express";
import cors from "cors";
import session from "express-session";
import passport from "passport";
import redis from "redis";
import createRedisStore from "connect-redis";
import EventEmitter from "events";

const RedisStore = createRedisStore(session);
const redisClient = redis.createClient({ url: process.env.REDIS_URL });

const app = express();

app.use(
  cors({
    origin: "http://localhost:3000",
    credentials: true,
  })
);

const PORT = isNaN(Number(process.env.PORT)) ? 5000 : Number(process.env.PORT);
const HOSTNAME = process.env.HOST ?? "127.0.0.1";

// temp: kafka
export const dataStream = new EventEmitter();

const { SESSION_SECRET } = process.env;

if (!SESSION_SECRET) {
  throw new Error("missing SESSION_SECRET env");
}

app.use(
  session({
    secret: SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    store: new RedisStore({ client: redisClient }),
  })
);
app.use(passport.initialize());
app.use(passport.session());

app.use("/user", userRouter);

DatabaseConnection.then(async () => {
  app.listen(PORT, HOSTNAME, () => {
    console.log(`Listening on port ${HOSTNAME}:${PORT}`);
  });
});
