import { User as UserModel } from "./entity/User";

declare global {
  namespace Express {
    interface User extends UserModel {}
  }
}
