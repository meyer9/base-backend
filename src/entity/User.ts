import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  BaseEntity,
  Index,
} from "typeorm";

@Entity()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  id!: string;

  @Column("varchar", { length: 64 })
  @Index({ unique: true })
  username!: string;

  /**
   * Password hash as Argon2 encoded form.
   */
  @Column("varchar", { length: 144 })
  passwordHash!: string;
}
