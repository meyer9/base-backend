import "reflect-metadata";
import { createConnection } from "typeorm";

export const DatabaseConnection = createConnection();
