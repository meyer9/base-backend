import { User } from "../entity/User";
import { getUserOrError } from "../middleware/auth";
import { Router } from "express";
import { json } from "body-parser";
import argon2 from "argon2";
import * as yup from "yup";
import passport from "passport";

const userRouter = Router();

userRouter.use(json());

const userCreation = yup
  .object()
  .required()
  .shape({
    username: yup.string().required().min(3).max(64),
    password: yup.string().required().min(8),
  });

userRouter.put("/", async (req, res) => {
  const { body } = req;

  if (!userCreation.isValidSync(body)) {
    res.json({ status: 400 });
    return;
  }

  const { username, password } = body;
  const passwordHash = await argon2.hash(password, {
    memoryCost: 128 * 1024,
    timeCost: 3,
  });

  const user = User.create({ username, passwordHash });

  await user.save();

  req.login(user, () => {
    res.json({ success: true });
  });
});

userRouter.post("/login", (req, res) =>
  passport.authenticate("local", (err, user) => {
    if (err) {
      return res.json({ success: false, message: err.message }).status(401);
    }
    if (!user) {
      return res.json({ success: false, message: err.message }).status(401);
    }
    req.logIn(user, function (err) {
      if (err) {
        return res.json({ success: false, message: err.message }).status(401);
      }
      return res.json({ success: true });
    });
  })(req, res)
);

userRouter.post("/logout", (req, res) => {
  req.logout?.();
  return res.json({ success: true });
});

userRouter.get("/", (req, res) => {
  const user = getUserOrError(req, res);
  if (!user) {
    return;
  }

  res.json(user);
});

export default userRouter;
