import { ValidationError, Validator } from "@toi/toi";

function validateTypes<TOutput>(
  body: unknown,
  validator: Validator<unknown, TOutput>
): { success: true; output: TOutput } | { success: false; message: string } {
  try {
    const out = validator(body);
    return { success: true, output: out };
  } catch (err) {
    if (err instanceof ValidationError) {
      return { success: false, message: err.message };
    } else {
      throw err;
    }
  }
}

export default validateTypes;
