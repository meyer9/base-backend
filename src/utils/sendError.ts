import { Response } from "express";

const sendError = (
  res: Response,
  status: number,
  message: string,
  context?: unknown
): void => {
  res.status(status);
  res.json({
    status,
    message,
    context,
  });
};

export default sendError;
