import { User } from "../entity/User";
import passport from "passport";
import { Strategy as LocalStrategy } from "passport-local";
import argon2 from "argon2";
import { Request, Response } from "express";

passport.use(
  new LocalStrategy(async (username, password, done) => {
    const user = await User.findOne({ username });

    if (!user) {
      return done(null, false, { message: "Invalid username" });
    }

    const valid = await argon2.verify(user.passwordHash, password);
    if (!valid) {
      return done(null, false, { message: "Invalid password" });
    }

    return done(null, user);
  })
);

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser(async (userId: string, done) => {
  const user = await User.findOne(userId);
  done(null, user);
});

export const getUserOrError = (req: Request, res: Response): User | null => {
  if (!req.user) {
    res.status(401).json({ status: 401, message: "Not logged in." });
    return null;
  }

  return req.user;
};
