declare global {
  namespace NodeJS {
    declare interface ProcessEnv {
      SESSION_SECRET?: string;
      REDIS_URL: string;
    }
  }
}
